package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/bradfitz/slice"
	redigo "github.com/garyburd/redigo/redis"
	"github.com/pressly/chi"
	"googlemaps.github.io/maps"
)

// OrderStatus ...
type OrderStatus int

var ORDER_PENDING OrderStatus = 1
var ORDER_ATTEMPTED OrderStatus = 2
var ORDER_DELIVERED OrderStatus = 3
var ORDER_IS_AGENT OrderStatus = 4

var myntraLatLng = maps.LatLng{
	Lat: 12.8925378,
	Lng: 77.64222440000003,
}

var agentID = "starlord"
var maxRadius = 11000
var fenceRadius = 100
var hookURL = "http://localhost:3000/endpoint"
var apiKey = "AIzaSyBFQQM85rJ8krCA8uS3qvHGEetPM1Q3woU"

// maximum no. of Waypoints allowed by google api
var maxCount = 21

var sortedOrders map[string]Order

var pendingOrderQueue []Order

var actions []Fulfill

// Fulfill ....
type Fulfill struct {
	OrderID string
	Action  string
}

// Order ...
type Order struct {
	ID           string      `json:"id"`
	LatLng       maps.LatLng `json:"latlng"`
	Status       OrderStatus `json:"status"`
	DistFromOrig float64     `json:"distFromOrig"`
}

// Bag ...
type Bag struct {
	AgentID      string      `json:"agentID"`
	DeliveryDate time.Time   `json:"deliveryDate"`
	Orders       []*Order    `json:"orders"`
	Origin       maps.LatLng `json:"origin"`
	OrderPrefix  string      `json:"orderPrefix"`
}

func (b *Bag) getOrders() []Order {

	c, err := redigo.Dial("tcp", ":9851")
	if err != nil {
		log.Fatalf("Could not connect: %v\n", err)
	}
	defer c.Close()

	ret, err := redigo.Values(c.Do("NEARBY", b.AgentID, "MATCH", b.OrderPrefix+"*",
		"DISTANCE", "POINTS", "POINT", b.Origin.Lat, b.Origin.Lng, maxRadius))

	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("%s", ret)
	//var nearby NearbyResult

	var outbound []Order

	outbound = append(outbound, Order{
		ID:           b.AgentID,
		DistFromOrig: 0,
		LatLng:       b.Origin,
		Status:       OrderStatus(ORDER_IS_AGENT),
	})

	orders := ret[1].([]interface{})
	for _, o := range orders {
		//fmt.Printf("%s\n", o)
		order := o.([]interface{})
		//fmt.Printf("%s\n", order)
		if "origin" == fmt.Sprintf("%s", order[0]) {
			continue
		}

		dist, err := strconv.ParseFloat(fmt.Sprintf("%s", order[3]), 64)
		if err != nil {
			log.Fatal("1", err)
		}

		latlng := order[1].([]interface{})

		lat, err := strconv.ParseFloat(fmt.Sprintf("%s", latlng[0]), 64)
		if err != nil {
			log.Fatal(err)
		}
		lng, err := strconv.ParseFloat(fmt.Sprintf("%s", latlng[1]), 64)
		if err != nil {
			log.Fatal(err)
		}

		status := order[2].([]interface{})
		st, err := strconv.Atoi(fmt.Sprintf("%s", status[1]))
		if err != nil {
			log.Fatal("2", err)
		}

		outbound = append(outbound, Order{
			ID:           fmt.Sprintf("%s", order[0]),
			DistFromOrig: dist,
			LatLng: maps.LatLng{
				Lat: lat,
				Lng: lng,
			},
			Status: OrderStatus(st),
		})

	}

	slice.Sort(outbound, func(i, j int) bool {
		return outbound[i].DistFromOrig < outbound[j].DistFromOrig
	})

	for _, v := range outbound {
		sortedOrders[v.ID] = v
	}

	return outbound

}

func (b *Bag) getDirectionRequests(orders []Order) []*maps.DirectionsRequest {

	var directionRequests []*maps.DirectionsRequest

	genDR := func(origin, dest int) {
		var path []maps.LatLng

		for _, o := range orders[origin:dest] {
			path = append(path, o.LatLng)
		}

		directionRequests = append(directionRequests, &maps.DirectionsRequest{
			Origin:      orders[origin].LatLng.String(),
			Destination: orders[dest].LatLng.String(),
			Waypoints:   []string{"optimize:true|enc:" + maps.Encode(path) + ":"},
		})

	}

	origin := 0
	dest := maxCount - 1

	if len(orders) < maxCount {
		dest = len(orders) - 1
		genDR(origin, dest)
		return directionRequests
	}

	for i := 0; i < len(orders); {

		genDR(origin, dest)

		i = i + maxCount

		if (len(orders) - i) < maxCount {
			origin = dest
			dest = len(orders) - 1
			genDR(origin, dest)
			break
		}

		origin = dest
		dest = dest + maxCount - 1

	}

	return directionRequests

}

func createBag(agentID string, orderPrefix string, origin maps.LatLng) *Bag {

	c, err := redigo.Dial("tcp", ":9851")
	if err != nil {
		log.Fatalf("Could not connect: %v\n", err)
	}
	defer c.Close()

	// set agent origin location
	_, err = c.Do("SET", "agents", agentID, "POINT", origin.Lat, origin.Lng)
	if err != nil {
		log.Fatal(err)
	}

	//log.Printf("agent %s origin set in db %v", agentID, res)

	// load orders
	file, err := os.Open("orders.csv")
	if err != nil {
		log.Println("Error:", err)
		return nil
	}
	defer file.Close()

	reader := csv.NewReader(file)

	coords, err := reader.ReadAll()
	if err != nil {
		log.Fatal("error setting bag origin", err)
	}

	var orders []*Order

	for i, v := range coords {
		if i == 0 {
			continue
		}

		lat, err := strconv.ParseFloat(v[0], 64)
		if err != nil {
			log.Fatal(err)
		}
		lng, err := strconv.ParseFloat(v[1], 64)
		if err != nil {
			log.Fatal(err)
		}

		// set order position in tile38
		_, err = c.Do("SET", agentID, orderPrefix+"_"+strconv.Itoa(i), "FIELD", "status", int(ORDER_PENDING), "POINT", lat, lng)
		if err != nil {
			log.Fatal("error setting order", err)
		}
		//log.Printf("order %d latlng set in db %v", i, res)

		// set order fence
		_, err = c.Do("SETHOOK", orderPrefix+"_"+strconv.Itoa(i)+"f", hookURL, "NEARBY",
			"agents", "FENCE", "DETECT", "inside,enter,exit,cross", "POINT", lat, lng, fenceRadius)
		if err != nil {
			log.Fatal("error setting order", err)
		}

		orders = append(orders, &Order{
			ID:     string(i),
			LatLng: maps.LatLng{Lat: lat, Lng: lng},
			Status: ORDER_PENDING,
		})
	}

	return &Bag{
		AgentID:      agentID,
		DeliveryDate: time.Now(),
		Orders:       orders,
		Origin:       origin,
	}

}

func staticMapURL(polyline string) string {

	surl, err := url.Parse("https://maps.googleapis.com/maps/api/staticmap")
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}

	params := url.Values{}
	params.Add("size", "600x400")
	params.Add("path", "color:0xff0000ff|weight:5|enc:"+polyline)
	params.Add("key", apiKey)

	surl.RawQuery = params.Encode()

	return surl.String()

}

func openBrowser(url string) bool {
	var args []string
	switch runtime.GOOS {
	case "darwin":
		args = []string{"open"}
	case "windows":
		args = []string{"cmd", "/c", "start"}
	default:
		args = []string{"xdg-open"}
	}
	cmd := exec.Command(args[0], append(args[1:], url)...)
	return cmd.Start() == nil
}

// HookMessage ...
type HookMessage struct {
	Command string `json:"command"`
	Detect  string `json:"detect"`
	Hook    string `json:"hook"`
	Time    string `json:"time"`
	Key     string `json:"key"`
	ID      string `json:"id"`
	Object  struct {
		Type        string    `json:"type"`
		Coordinates []float64 `json:"coordinates"`
	} `json:"object"`
}

func startHookListener() {
	r := chi.NewRouter()

	r.Post("/endpoint", func(w http.ResponseWriter, r *http.Request) {

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println("error reading request body", err)
			return
		}

		// The very useful unmarshal method decodes the incoming json onto the request
		// type
		var hookMessage HookMessage
		err = json.Unmarshal(body, &hookMessage)
		if err != nil {
			log.Println("json unmarshal error", err)
			return
		}
		log.Println("Got HookMessage:", hookMessage)

		if hookMessage.Detect == "exit" {
			takeAction(strings.TrimRight(hookMessage.ID, "f"))
		}

		w.WriteHeader(http.StatusOK)
	})
	http.ListenAndServe(":3000", r)

}

func takeAction(orderID string) {

	for _, a := range actions {
		if a.OrderID == orderID {
			pendingOrderQueue = append(pendingOrderQueue, sortedOrders[orderID])
		}
	}
}

func updateAgentLocation(order Order) {
	c, err := redigo.Dial("tcp", ":9851")
	if err != nil {
		log.Fatalf("Could not connect: %v\n", err)
	}

	// set agent origin location
	ret, err := c.Do("SET", "agents", agentID, "POINT", order.LatLng.Lat, order.LatLng.Lng)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("updated agent ", ret)
}

func agentSimulation() {

	for _, o := range sortedOrders {

		updateAgentLocation(o)
		<-time.After(time.Second * 5)

	}

	if len(pendingOrderQueue) <= 0 {
		return
	}

	// otherwise plan a return journey

	// reverse array
	for i := len(pendingOrderQueue)/2 - 1; i >= 0; i-- {
		opp := len(pendingOrderQueue) - 1 - i
		pendingOrderQueue[i], pendingOrderQueue[opp] = pendingOrderQueue[opp], pendingOrderQueue[i]
	}

	b := Bag{}
	generateRoute(b.getDirectionRequests(pendingOrderQueue))

	time.Sleep(time.Second * 2)

	for _, o := range pendingOrderQueue {
		updateAgentLocation(o)
		<-time.After(time.Second * 5)
	}

}

func generateRoute(directionRequests []*maps.DirectionsRequest) {

	c, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}

	for i, r := range directionRequests {
		resp, _, err := c.Directions(context.Background(), r)
		if err != nil {
			log.Fatalf("fatal error: %s", err)
		}

		for j, route := range resp {
			imageURL := staticMapURL(route.OverviewPolyline.Points)
			fmt.Println("Leg", i, "Route", j, imageURL)
			openBrowser(imageURL)
		}

	}

}

func main() {

	// load orders
	file, err := os.Open("fulfill.csv")
	if err != nil {
		log.Fatal("Error:", err)
	}
	defer file.Close()

	reader := csv.NewReader(file)

	fulfill, err := reader.ReadAll()
	if err != nil {
		log.Fatal("error setting bag origin", err)
	}

	for _, f := range fulfill {
		actions = append(actions, Fulfill{OrderID: f[0], Action: f[1]})
	}

	sortedOrders = make(map[string]Order)
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	go func() {
		<-sigs
		done <- true
	}()

	go startHookListener()

	b := createBag(agentID, "order", myntraLatLng)
	directionRequests := b.getDirectionRequests(b.getOrders())

	generateRoute(directionRequests)

	time.Sleep(time.Second * 2)
	go agentSimulation()

	fmt.Println("Ctrl-C to interrupt...")
	<-done
	fmt.Println("Exiting...")

}
