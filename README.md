## Samaritan: Using geofencing and waypoints for Route Optimization.

A geofence is a shape around a geolocation used to trigger events such as: enter, exit, cross etc. w.r.t to a moving object
or a delivery agent.

The ideas is to use geofencing around a delivery loction as a checklist to do the following:

1. Check order status: delivered, attempted for delivery etc. when agent "crosses" a geofence.
2. Check if order was delivered according to the generated route or picked arbitrarily. 
3. Queue order for return journey route if the order was attempted for delivery.
4. Remove order from route and regenerate route, if order delivery was cancelled.
5. Help agent for directions if too much time being spent "near" a geofence. This can help in detecting possible address
    confusion. 
6. Alert customer that a delivery agent is nearby. Can help in faster drop-off. 



Steps:

0. Get all orders allotted to the agent in a 10 km radius and sort them by distance from origin.
1. Save Order location in db for route generation using Google Maps Directions API.
2. Each order then becomes a waypoint.
3. Then we create a bag of orders sorted by farthest from origin. If number of order is greater than 23, need to split it into
   multiple "bag runs" to overcome the waypoints limits in Google Maps Directions API.
4. The orders list is used to generate route using the Directions API 
4. We also create a geofence of 100 metres around each order. 
5. As the agent exists a "order geofence", we trigger a check if the order was delivered. If not, then we put the order in a 
    return journey queue.
6. As the agent reaches the last order, we generate a new route for the return journey.

Dependency: tile38.com